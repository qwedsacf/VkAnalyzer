﻿using MongoDB.Bson;

namespace VkAnalyzer.BE.Mongo
{
	public class MongoTrackerSettings : TrackerSettings
	{
		public ObjectId Id { get; set; }
	}
}
