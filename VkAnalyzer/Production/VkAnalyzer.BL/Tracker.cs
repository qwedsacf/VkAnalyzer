﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VkAnalyzer.BE;
using VkAnalyzer.Interfaces;
using VkNet.Exception;

namespace VkAnalyzer.BL
{
	public class Tracker : ITracker
	{
		public int UsersCountPerTime { get; set; } = 1000;
		public int TimerPeriod { get; set; } = 10000;

		private readonly IUserInfoSource _userInfoSource;
		private readonly IUserInfoRepository _userRepository;
		private readonly ILogger _logger;

		private readonly HashSet<long> _usersQueue;
		private readonly Dictionary<long, CheckInfo> _lastInfo = new Dictionary<long, CheckInfo>();
		private Timer _timer;

		public Tracker(IUserInfoSource userInfoSource, IUserInfoRepository userRepository, ILogger logger, HashSet<long> users = null)
		{
			_userInfoSource = userInfoSource ?? throw new ArgumentNullException(nameof(userInfoSource));
			_userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
			_usersQueue = users ?? new HashSet<long>();
		}

		public Task Start()
		{
			_timer = new Timer(UpdateInfo, null, 0, TimerPeriod);
			return Task.CompletedTask;
		}

		private async void UpdateInfo(object state)
		{
			try
			{
				var index = 0;
				if (_usersQueue.Count == 0)
				{
					return;
				}

				var users = _usersQueue.ToList();
				var infos = new List<UserOnlineInfo>();

				while (true)
				{
					var from = index * UsersCountPerTime;

					if (from > _usersQueue.Count - 1)
					{
						break;
					}

					var currentUsers = users.Skip(from).Take(UsersCountPerTime).ToList();

					IEnumerable<UserOnlineInfo> userInfos;

					try
					{
						userInfos = (await _userInfoSource.GetOnlineInfo(currentUsers)).ToList();
					}
					catch (TooManyRequestsException ex)
					{
						Debug.WriteLine("Too many requests");
						await _logger.LogAsync(ex, "Tracker Too Many Requests Exception");
						continue;
					}
					catch (Exception ex)
					{
						await _logger.LogAsync(ex, "Tracker Inner Exception");
						await HandleException(currentUsers);
						index++;
						continue;
					}

					var updateList = new List<UserOnlineInfo>();

					foreach (var info in userInfos)
					{
						if (!IsLastInfoDifferent(info)) continue;

						_lastInfo[info.Id] = new CheckInfo
						{
							OnlineInfo = info.OnlineInfo,
							LastCheck = info.DateTime
						};
						updateList.Add(info);
					}

					infos.AddRange(updateList);

					index++;
				}

				if (!infos.Any()) return;

				await _userRepository.SaveDataAsync(infos);
			}
			catch (Exception ex)
			{
				await _logger.LogAsync(ex, "Tracker Top Exception");
			}
		}

		private async Task HandleException(IEnumerable<long> currentUsers)
		{
			// check lastInfo and set Undefined status if needed
			var overdueList = currentUsers
				.Where(u => DateTime.Now - _lastInfo[u].LastCheck > TimeSpan.FromSeconds(10 * TimerPeriod))
				.Select(u => new UserOnlineInfo
				{
					Id = u,
					OnlineInfo = OnlineInfo.Undefined,
					DateTime = DateTime.Now
				})
				.ToList();

			if (overdueList.Any())
			{
				foreach (var info in overdueList)
				{
					_lastInfo[info.Id] = new CheckInfo
					{
						OnlineInfo = info.OnlineInfo,
						LastCheck = info.DateTime
					};
				}

				await _userRepository.SaveDataAsync(overdueList);
			}
		}

		private bool IsLastInfoDifferent(UserOnlineInfo info)
		{
			return !_lastInfo.ContainsKey(info.Id) || _lastInfo[info.Id].OnlineInfo != info.OnlineInfo;
		}

		public void AddUsers(IEnumerable<long> userIds)
		{
			foreach (var userId in userIds)
			{
				_usersQueue.Add(userId);
			}
		}

		public bool RemoveUser(long userId)
		{
			return _usersQueue.Remove(userId);
		}

		public void Stop()
		{
			_timer.Dispose();
		}

		public IEnumerable<long> GetUsers()
		{
			return _usersQueue;
		}

		private struct CheckInfo
		{
			public OnlineInfo OnlineInfo { get; set; }
			public DateTime LastCheck { get; set; }
		}
	}
}
