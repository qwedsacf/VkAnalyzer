﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VkAnalyzer.BE;
using VkAnalyzer.Interfaces;
using VkAnalyzer.WebApp.Models;

namespace VkAnalyzer.WebApp.Controllers
{
	/// <inheritdoc />
	/// <summary />
	[Route("api/[controller]")]
	[ApiController]
	public class AccountController : ControllerBase
	{
		private readonly IUserRepository _userRepository;
		private readonly IAccountRepository _accountRepository;

		public Guid Id
		{
			get
			{
				var cookieExist = ControllerContext.HttpContext.Request.Cookies.TryGetValue("Id", out var idString);

				if (cookieExist && Guid.TryParse(idString, out var result)) return result;

				result = Guid.NewGuid();
				ControllerContext.HttpContext.Response.Cookies.Append("Id", result.ToString(), new CookieOptions
				{
					Expires = DateTimeOffset.Now.AddYears(1)
				});

				return result;
			}

		}

		/// <summary />
		/// <param name="userRepository"></param>
		/// <param name="accountRepository"></param>
		public AccountController(IUserRepository userRepository, IAccountRepository accountRepository)
		{
			_userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
			_accountRepository = accountRepository ?? throw new ArgumentNullException(nameof(accountRepository));
		}

		/// <summary />
		/// <param name="request"></param>
		/// <returns></returns>
		[HttpPost("addUsers")]
		public async Task<BaseResponse> AddUsers([FromBody] AddUsersRequest request)
		{
			await _accountRepository.AddTrackedUsersAsync(Id, request.Ids);
			return new BaseSuccessResponse();
		}

		/// <summary />
		/// <returns></returns>
		[HttpGet("trackedUsers")]
		public async Task<BaseResponse<IEnumerable<User>>> GetTrackedUsers()
		{
			var userIds = await _accountRepository.GetTrackedUsersAsync(Id);
			return new BaseSuccessResponse<IEnumerable<User>>
			{
				Data = userIds == null
					? Enumerable.Empty<User>()
					: await _userRepository.GetUsersAsync(userIds)
			};
		}
	}
}
