﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace VkAnalyzer.UnitTests
{
	public static class TestHelpers
	{
		public static void CompareCollections<T>(IEnumerable<T> expected, IEnumerable<T> actual)
		{
			var expectedArray = expected.ToArray();
			var actualArray = actual.ToArray();
			Assert.AreEqual(expectedArray.Length, actualArray.Length);
			for (var i = 0; i < expectedArray.Length; i++)
			{
				Assert.AreEqual(expectedArray[i], actualArray[i]);
			}
		}
	}
}
