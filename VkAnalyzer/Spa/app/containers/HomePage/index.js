import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose, bindActionCreators } from 'redux';

import { Grid, Button } from 'semantic-ui-react';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import {
  makeSelectHomePage,
  makeSelectUserOnlineData,
  makeSelectUsersCount,
  makeSelectUserName,
  makeSelectUser,
  makeSelectDailyOnlineDuration,
  makeSelectWeeklyOnlineDuration,
  makeSelectFilteredUsers,
  makeSelectAllOnlineDuration,
} from './selectors';
import {
  getUsers,
  getData,
  addFriends,
  usernameChanged,
  usersFilterChanged,
} from './actions';
import reducer from './reducer';
import saga from './saga';
import { MainContainer, MainTitle, SectionsDivider } from './Wrappers';
import Block from '../../components/Block';
import UserInfo from '../../components/UserInfo';
import AddUsers from '../AddUser';
import OnlineChartSection from './OnlineChartSection';
import { getOnlineDurationString } from '../../utils/dateTime';
import SelectUsers from '../../components/SelectUsers';

class HomePage extends React.Component {
  componentDidMount() {
    const { getUsers } = this.props;
    getUsers();
  }

  selectUser = id => {
    const { getData } = this.props;
    getData(id);
  };

  render() {
    const {
      usersCount,
      userOnlineData,
      getData,
      // getFriends,
      user,
      dailyDuration,
      weeklyDuration,
      allDuration,
      usersFilterChanged,
      filteredUsers,
      addFriends,
      homePage,
    } = this.props;

    const onlineData = userOnlineData.toJS();

    const selectUsersProps = {
      users: filteredUsers.users,
      totalPages: filteredUsers.totalPages,
      onFilterChange: usersFilterChanged,
      onSelectUser: this.selectUser,
      loading: homePage.get('loading'),
    };

    return (
      <div>
        <Helmet>
          <title>HomePage</title>
          <meta name="description" content="HomePage" />
        </Helmet>
        <MainContainer>
          <MainTitle as="h1">Статистика посещаемости ВКонтакте</MainTitle>

          <SectionsDivider horizontal>Статистика</SectionsDivider>
          <div style={{ marginBottom: '20px' }}>
            Количество пользователей: {usersCount}
          </div>

          <Grid>
            <Grid.Column computer={8} mobile={16}>
              <SelectUsers {...selectUsersProps} />
            </Grid.Column>
            <Grid.Column computer={8} mobile={16}>
              {user && (
                <>
                  <Grid>
                    <Grid.Column width={5}>
                      <UserInfo user={user} />
                    </Grid.Column>
                    <Grid.Column width={11}>
                      {user.get('friendsCount') && (
                        <Block>Друзей: {user.get('friendsCount')}</Block>
                      )}

                      {dailyDuration > 0 && (
                        <Block>
                          Время в сети за сегодня:{' '}
                          {getOnlineDurationString(dailyDuration)}
                        </Block>
                      )}

                      {weeklyDuration > 0 && (
                        <Block>
                          Время в сети за неделю:{' '}
                          {getOnlineDurationString(weeklyDuration)}
                        </Block>
                      )}

                      {allDuration > 0 && (
                        <Block>
                          Время в сети за все время:{' '}
                          {getOnlineDurationString(allDuration)}
                        </Block>
                      )}

                      <Block>
                        <Button
                          onClick={() => addFriends(user.get('id'))}
                          content="Добавить друзей"
                        />
                      </Block>
                    </Grid.Column>
                  </Grid>

                  <Grid>
                    <Grid.Column width={16}>
                      <OnlineChartSection
                        onlineData={onlineData}
                        getData={(from, to) =>
                          getData(user.get('id'), from, to)
                        }
                      />
                    </Grid.Column>
                  </Grid>
                </>
              )}
            </Grid.Column>
          </Grid>

          <SectionsDivider horizontal>Добавление пользователей</SectionsDivider>
          <AddUsers />
        </MainContainer>
      </div>
    );
  }
}

HomePage.propTypes = {
  // selectors
  homePage: PropTypes.object,
  users: PropTypes.object,
  usersCount: PropTypes.number,
  userOnlineData: PropTypes.object,
  userName: PropTypes.string,
  user: PropTypes.object,
  dailyDuration: PropTypes.number,
  weeklyDuration: PropTypes.number,
  usersTotalPages: PropTypes.number,
  filteredUsers: PropTypes.object,

  // actions
  getUsers: PropTypes.func,
  getData: PropTypes.func,
  addFriends: PropTypes.func,
  usernameChanged: PropTypes.func,
  usersFilterChanged: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  homePage: makeSelectHomePage(),
  usersCount: makeSelectUsersCount(),
  userOnlineData: makeSelectUserOnlineData(),
  userName: makeSelectUserName(),
  user: makeSelectUser(),
  dailyDuration: makeSelectDailyOnlineDuration(),
  weeklyDuration: makeSelectWeeklyOnlineDuration(),
  allDuration: makeSelectAllOnlineDuration(),
  filteredUsers: makeSelectFilteredUsers(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getUsers,
      getData,
      addFriends,
      usernameChanged,
      usersFilterChanged,
    },
    dispatch,
  );
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'homePage', reducer });
const withSaga = injectSaga({ key: 'homePage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
