﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using VkAnalyzer.BE;
using VkAnalyzer.BE.Mongo;
using VkAnalyzer.Interfaces;

namespace VkAnalyzer.BL
{

	public class MongoRepository : IUserInfoRepository, IUserRepository, ITrackerRepository, IAccountRepository, ILogger
	{
		private const int DefaultMongoPort = 27017;
		private readonly IMongoDatabase _db;
		private IMongoCollection<MongoUser> Users => _db.GetCollection<MongoUser>("users");
		private IMongoCollection<MongoTrackerSettings> Trackers => _db.GetCollection<MongoTrackerSettings>("trackers");
		private IMongoCollection<MongoAccount> Accounts => _db.GetCollection<MongoAccount>("accounts");
		private IMongoCollection<MongoLog> Logs => _db.GetCollection<MongoLog>("logs");

		public MongoRepository(MongoConnectionSettings connectionSettings)
		{
			var parsed = int.TryParse(connectionSettings.Port, out var port);
			var settings = new MongoClientSettings
			{
				Server = new MongoServerAddress(connectionSettings.Host, parsed ? port : DefaultMongoPort)
			};

			if (!string.IsNullOrEmpty(connectionSettings.Login))
			{
				settings.Credential = MongoCredential.CreateCredential("admin", connectionSettings.Login,
					connectionSettings.Password);
			}

			var client = new MongoClient(settings);

			_db = client.GetDatabase(connectionSettings.Database);

			if (!CollectionExists(_db, "users"))
			{
				_db.CreateCollection("users");
			}

			if (!CollectionExists(_db, "trackers"))
			{
				_db.CreateCollection("trackers");
			}

			if (!CollectionExists(_db, "accounts"))
			{
				_db.CreateCollection("accounts");
			}

			if (!CollectionExists(_db, "logs"))
			{
				_db.CreateCollection("logs");
			}
		}

		public bool CollectionExists(IMongoDatabase database, string collectionName)
		{
			var filter = new BsonDocument("name", collectionName);
			var options = new ListCollectionNamesOptions { Filter = filter };

			return database.ListCollectionNames(options).Any();
		}

		#region Implementation of IUsersInfoRepository

		public UserOnlineData ReadData(long id, DateTime from, DateTime to)
		{
			var data = Users.Find(u => u.Id == id).FirstOrDefault();

			if (data == null)
				throw new BaseApiException("Пользователь с указанным идентификатором не найден");

			var infos = data.Info.Where(i => i.DateTime >= from && i.DateTime <= to).ToList();

			var lastBeforeFrom = data.Info.ToList().FindLast(i => i.DateTime < from);

			if (lastBeforeFrom != null)
			{
				lastBeforeFrom.DateTime = from;
				infos.Insert(0, lastBeforeFrom);
			}

			var lastInfo = infos.LastOrDefault();
			if (lastInfo != null && (lastInfo.OnlineInfo == OnlineInfo.Online || lastInfo.OnlineInfo == OnlineInfo.OnlineMobile))
			{
				infos.Add(new MongoOnlineInfo { DateTime = to, OnlineInfo = OnlineInfo.Offline });
			}

			return new UserOnlineData
			{
				Id = id,
				OnlineInfos = infos.Select(i => new DateOnline
				{
					OnlineInfo = i.OnlineInfo,
					Date = i.DateTime
				})
			};
		}
		
		public async Task<UserOnlineData> ReadDataAsync(long id, DateTime from, DateTime to)
		{
			var data = (await Users.FindAsync(u => u.Id == id)).FirstOrDefault();

			if (data == null)
				throw new BaseApiException("Пользователь с указанным идентификатором не найден");

			var infos = data.Info.Where(i => i.DateTime >= from && i.DateTime <= to).ToList();

			var lastBeforeFrom = data.Info.ToList().FindLast(i => i.DateTime < from);

			if (lastBeforeFrom != null)
			{
				lastBeforeFrom.DateTime = from;
				infos.Insert(0, lastBeforeFrom);
			}

			var lastInfo = infos.LastOrDefault();
			if (lastInfo != null && (lastInfo.OnlineInfo == OnlineInfo.Online || lastInfo.OnlineInfo == OnlineInfo.OnlineMobile))
			{
				infos.Add(new MongoOnlineInfo { DateTime = to, OnlineInfo = OnlineInfo.Offline });
			}

			return new UserOnlineData
			{
				Id = id,
				OnlineInfos = infos.Select(i => new DateOnline
				{
					OnlineInfo = i.OnlineInfo,
					Date = i.DateTime
				})
			};
		}

		public void SaveData(IEnumerable<UserOnlineInfo> infos)
		{
			foreach (var userOnlineInfo in infos)
			{
				var update = Builders<MongoUser>.Update.Push(u => u.Info, new MongoOnlineInfo
				{
					DateTime = userOnlineInfo.DateTime,
					OnlineInfo = userOnlineInfo.OnlineInfo
				});

				Users.UpdateOne(u => u.Id == userOnlineInfo.Id, update);
			}
		}

		public async Task SaveDataAsync(IEnumerable<UserOnlineInfo> infos)
		{
			foreach (var userOnlineInfo in infos)
			{
				var update = Builders<MongoUser>.Update.Push(u => u.Info, Map(userOnlineInfo));

				await Users.UpdateOneAsync(u => u.Id == userOnlineInfo.Id, update);
			}
		}

		#endregion

		#region Implementation of IUserRepository

		public int GetUsersCount()
		{
			return (int)Users.CountDocuments(Builders<MongoUser>.Filter.Empty);
		}

		public async Task<int> GetUsersCountAsync()
		{
			return (int)await Users.CountDocumentsAsync(Builders<MongoUser>.Filter.Empty);
		}

		public IEnumerable<User> GetUsers(IEnumerable<long> ids = null)
		{
			var filter = ids == null
				? Builders<MongoUser>.Filter.Empty
				: Builders<MongoUser>.Filter.Where(u => ids.Contains(u.Id));

			return Users.FindSync(filter).ToList().Select(Map);
		}

		public async Task<IEnumerable<User>> GetUsersAsync(IEnumerable<long> ids = null)
		{
			var filter = ids == null
				? Builders<MongoUser>.Filter.Empty
				: Builders<MongoUser>.Filter.Where(u => ids.Contains(u.Id));

			return (await Users.FindAsync(filter)).ToList().Select(Map);
		}

		public void AddUser(User user)
		{
			Users.InsertOne(Map(user));
		}

		public async Task AddUserAsync(User user)
		{
			await Users.InsertOneAsync(Map(user));
		}

		public bool IsUserTracked(long id)
		{
			return Users.Find(u => u.Id == id).Any();
		}

		public async Task<bool> IsUserTrackedAsync(long id)
		{
			return (await Users.FindAsync(u => u.Id == id)).Any();
		}

		private static User Map(MongoUser user)
		{
			return new User
			{
				Id = user.Id,
				FirstName = user.FirstName,
				LastName = user.LastName,
				AddedDateTime = user.AddedDateTime,
				ScreenName = user.ScreenName,
				AdditionalInfo = user.AdditionalInfo,
				Photo = user.Photo,
				LastOnline = user.LastOnline
			};
		}

		private static MongoUser Map(User user)
		{
			return new MongoUser
			{
				Id = user.Id,
				FirstName = user.FirstName,
				LastName = user.LastName,
				AddedDateTime = user.AddedDateTime,
				AdditionalInfo = user.AdditionalInfo,
				Photo = user.Photo,
				ScreenName = user.ScreenName,
				Info = new List<MongoOnlineInfo>
				{
					new MongoOnlineInfo {DateTime = DateTime.Now, OnlineInfo = OnlineInfo.Undefined}
				}
			};
		}

		private static MongoOnlineInfo Map(UserOnlineInfo userOnlineInfo)
		{
			return new MongoOnlineInfo
			{
				DateTime = userOnlineInfo.DateTime,
				OnlineInfo = userOnlineInfo.OnlineInfo
			};
		}

		#endregion

		#region Implementation of ITrackerRepository

		public IEnumerable<TrackerSettings> GetTrackers()
		{
			return Trackers.Find(x => true).ToList();
		}

		public async Task<IEnumerable<TrackerSettings>> GetTrackersAsync()
		{
			return await Trackers.Find(x => true).ToListAsync();
		}

		public void AddTrackers(IEnumerable<TrackerSettings> trackers)
		{
			Trackers.InsertMany(trackers.Select(Map));
		}

		public async Task AddTrackersAsync(IEnumerable<TrackerSettings> trackers)
		{
			await Trackers.InsertManyAsync(trackers.Select(Map));
		}

		private static MongoTrackerSettings Map(TrackerSettings tracker)
		{
			return new MongoTrackerSettings
			{
				AppId = tracker.AppId,
				Login = tracker.Login,
				Password = tracker.Password
			};
		}

		#endregion

		#region Implementation of IAccountRepository

		public IEnumerable<long> GetTrackedUsers(Guid id)
		{
			return Accounts.FindSync(a => a.Id == id).FirstOrDefault()?.TrackedUsers;
		}

		public async Task<IEnumerable<long>> GetTrackedUsersAsync(Guid id)
		{
			return (await Accounts.FindAsync(a => a.Id == id)).FirstOrDefault()?.TrackedUsers;
		}

		public void AddTrackedUsers(Guid id, IEnumerable<long> users)
		{
			Accounts.UpdateOne(a => a.Id == id,
				Builders<MongoAccount>.Update.AddToSetEach(item => item.TrackedUsers, users),
				new UpdateOptions { IsUpsert = true });
		}

		public async Task AddTrackedUsersAsync(Guid id, IEnumerable<long> users)
		{
			await Accounts.UpdateOneAsync(a => a.Id == id,
				Builders<MongoAccount>.Update.AddToSetEach(item => item.TrackedUsers, users),
				new UpdateOptions { IsUpsert = true });
		}

		#endregion

		#region Implementation of ILogger

		public void Log(Exception exception, string additionalInfo = null)
		{
			Logs.InsertOne(new MongoLog
			{
				DateTime = DateTime.Now,
				Message = exception.Message,
				StackTrace = exception.StackTrace,
				AdditionalInfo = additionalInfo
			});
		}

		public async Task LogAsync(Exception exception, string additionalInfo = null)
		{
			await Logs.InsertOneAsync(new MongoLog
			{
				DateTime = DateTime.Now,
				Message = exception.Message,
				StackTrace = exception.StackTrace,
				AdditionalInfo = additionalInfo
			});
		}

		#endregion
	}
}
