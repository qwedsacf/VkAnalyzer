import { createSelector } from 'reselect';
import dateFns from 'date-fns';
import { initialState } from './reducer';

const selectHomePageDomain = state => state.get('homePage', initialState);

const makeSelectHomePage = () =>
  createSelector(selectHomePageDomain, substate => substate);

const makeSelectAllUsers = () =>
  createSelector(selectHomePageDomain, substate => substate.get('users'));

const makeSelectFilteredUsers = () =>
  createSelector(selectHomePageDomain, substate => {
    const users = substate.get('users');
    if (!(users && users.size)) return { users: null, totalPages: 0 };

    const filter = substate.get('usersFilter');
    const name = filter.get('name') || '';

    const begin = (filter.get('page') - 1) * 10;
    const end = begin + 10;

    const filteredUsers = users.filter(u =>
      `${u.get('firstName').toLowerCase()} ${u
        .get('lastName')
        .toLowerCase()}`.includes(name.toLowerCase()),
    );

    return {
      users: filteredUsers.slice(begin, end),
      totalPages: Math.ceil(filteredUsers.size / 10),
    };
  });

const makeSelectUsersCount = () =>
  createSelector(selectHomePageDomain, substate =>
    substate.getIn(['stat', 'usersCount']),
  );

const makeSelectUserOnlineData = () =>
  createSelector(selectHomePageDomain, substate =>
    substate.get('userOnlineData'),
  );

const makeSelectUserName = () =>
  createSelector(selectHomePageDomain, substate => substate.get('userName'));

const makeSelectUser = () =>
  createSelector(selectHomePageDomain, substate => substate.get('user'));

const getOnlineDuration = (data, from, to) => {
  if (!data) return null;
  const filtered = data
    .map(item => {
      let startTime = new Date(item.get('startTime'));
      let endTime = new Date(item.get('endTime'));

      if (endTime < from || startTime > to) return null;

      startTime = dateFns.max(startTime, from);
      endTime = dateFns.min(endTime, to);

      return item.set('startTime', startTime).set('endTime', endTime);
    })
    .filter(item => item !== null);
  const result = filtered.reduce(
    (prev, current) =>
      prev + (current.get('endTime') - current.get('startTime')),
    0,
  );
  return result;
};

const makeSelectAllOnlineDuration = () =>
  createSelector(selectHomePageDomain, substate => {
    const data = substate.getIn(['userOnlineData', 'data', 'onlineInfos']);
    if (!data) return null;
    const result = data.reduce(
      (prev, current) =>
        prev +
        (new Date(current.get('endTime')) - new Date(current.get('startTime'))),
      0,
    );
    return result;
  });

const makeSelectDailyOnlineDuration = () =>
  createSelector(selectHomePageDomain, substate => {
    const data = substate.getIn(['userOnlineData', 'data', 'onlineInfos']);
    if (!data) return null;
    const startOfDay = dateFns.startOfDay(new Date());
    const endOfDay = dateFns.endOfDay(new Date());
    const result = getOnlineDuration(data, startOfDay, endOfDay);
    return result;
  });

const makeSelectWeeklyOnlineDuration = () =>
  createSelector(selectHomePageDomain, substate => {
    const data = substate.getIn(['userOnlineData', 'data', 'onlineInfos']);
    if (!data) return null;
    const startOfWeek = dateFns.startOfWeek(new Date(), { weekStartsOn: 1 });
    const endOfWeek = dateFns.endOfWeek(new Date(), { weekStartsOn: 1 });
    const result = getOnlineDuration(data, startOfWeek, endOfWeek);
    return result;
  });

export {
  makeSelectHomePage,
  makeSelectUsersCount,
  makeSelectUserOnlineData,
  makeSelectUserName,
  makeSelectAllUsers,
  makeSelectUser,
  makeSelectAllOnlineDuration,
  makeSelectDailyOnlineDuration,
  makeSelectWeeklyOnlineDuration,
  makeSelectFilteredUsers,
};
