import { fromJS } from 'immutable';
import {
  FIND_USERS_SUCCESS,
  FIND_USERS_ERROR,
  ADD_USER_SUCCESS,
} from './constants';

export const initialState = fromJS({
  loading: false,
  error: null,
  foundedUsers: null,
  userAdded: false,
});

function homePageReducer(state = initialState, action) {
  switch (action.type) {
    case FIND_USERS_SUCCESS:
      return state.set('foundedUsers', fromJS(action.data.users));
    case FIND_USERS_ERROR:
      return state.set('foundedUsers', fromJS([]));

    case ADD_USER_SUCCESS:
      return state.set('userAdded', true);

    default:
      return state;
  }
}

export default homePageReducer;
