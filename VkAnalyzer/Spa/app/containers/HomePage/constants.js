export const GET_USERS = 'app/HomePage/GET_USERS';
export const SET_USERS = 'app/HomePage/SET_USERS';
export const GET_USERS_ERROR = 'app/HomePage/GET_USERS_ERROR';

export const GET_DATA = 'app/HomePage/GET_DATA';
export const GET_DATA_SUCCESS = 'app/HomePage/GET_DATA_SUCCESS';
export const GET_DATA_ERROR = 'app/HomePage/GET_DATA_ERROR';

export const ADD_FRIENDS = 'app/HomePage/ADD_FRIENDS';
export const SET_FRIENDS = 'app/HomePage/SET_FRIENDS';
export const GET_FRIENDS_ERROR = 'app/HomePage/GET_FRIENDS_ERROR';

export const SET_USERS_COUNT = 'app/HomePage/SET_USERS_COUNT';

export const USERNAME_CHANGED = 'app/HomePage/USERNAME_CHANGED';

export const GET_USER_INFO = 'app/HomePage/GET_USER_INFO';
export const SET_USER_INFO = 'app/HomePage/SET_USER_INFO';
export const GET_USER_INFO_ERROR = 'app/HomePage/GET_USER_INFO_ERROR';

export const USERS_FILTER_CHANGED = 'app/HomePage/USERS_FILTER_CHANGED';
