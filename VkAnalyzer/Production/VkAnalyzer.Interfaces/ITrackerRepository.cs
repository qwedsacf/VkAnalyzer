﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VkAnalyzer.BE;

namespace VkAnalyzer.Interfaces
{
	public interface ITrackerRepository
	{
		IEnumerable<TrackerSettings> GetTrackers();
		Task<IEnumerable<TrackerSettings>> GetTrackersAsync();

		void AddTrackers(IEnumerable<TrackerSettings> trackers);
		Task AddTrackersAsync(IEnumerable<TrackerSettings> trackers);
	}
}
