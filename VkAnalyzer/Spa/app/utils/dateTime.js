import moment from 'moment';
import { getNoun } from './wordCase';

export const getOnlineDurationString = duration => {
  const d = moment.duration(duration);

  const minutes = d.minutes();
  const hours = d.hours();
  const days = d.days();
  const weeks = d.weeks();
  const months = d.months();

  if (d.asMinutes() < 1) return 'меньше минуты';

  let result = '';

  if (months > 0)
    result += `${months} ${getNoun(months, 'месяц', 'месяца', 'месяцев')} `;

  if (weeks > 0)
    result += `${weeks} ${getNoun(weeks, 'неделя', 'недели', 'недель')} `;

  if (days > 0) result += `${days} ${getNoun(days, 'день', 'дня', 'дней')} `;

  if (hours > 0)
    result += `${hours} ${getNoun(hours, 'час', 'часа', 'часов')} `;

  if (minutes > 0)
    result += `${minutes} ${getNoun(minutes, 'минута', 'минуты', 'минут')}`;

  return result;
};
