﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace VkAnalyzer.BE.Mongo
{
	public class MongoAccount
	{
		[BsonId]
		public Guid Id { get; set; }
		public IEnumerable<long> TrackedUsers { get; set; }
	}
}
