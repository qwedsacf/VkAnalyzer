﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace VkAnalyzer.BE.Mongo
{
	[BsonIgnoreExtraElements]
	public class MongoUser
	{
		[BsonId]
		public long Id { get; set; }

		public string FirstName { get; set; }
		public string LastName { get; set; }

		public DateTime? AddedDateTime { get; set; }

		public string Photo { get; set; }
		public string ScreenName { get; set; }
		public DateTime? LastOnline { get; set; }
		public Dictionary<string, string> AdditionalInfo { get; set; }

		public Guid? AddedUser { get; set; }

		public IEnumerable<MongoOnlineInfo> Info { get; set; }
	}
}