import React from 'react';
import PropTypes from 'prop-types';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import { DatePickerWrapper } from './Wrappers';

const DateTimePicker = ({ selected, onChange }) => (
  <DatePickerWrapper>
    <DatePicker
      selected={selected}
      onChange={onChange}
      dateFormat="dd.MM.YYYY HH:mm"
      locale="ru"
      placeholderText="До"
      showTimeSelect
      timeFormat="HH:mm"
      timeIntervals={15}
      timeCaption="Время"
      isClearable
    />
  </DatePickerWrapper>
);

DateTimePicker.propTypes = {
  selected: PropTypes.oneOf([undefined, PropTypes.date]),
  onChange: PropTypes.func,
};

export default DateTimePicker;
