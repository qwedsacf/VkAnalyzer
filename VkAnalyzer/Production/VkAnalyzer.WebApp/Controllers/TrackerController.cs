﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VkAnalyzer.BE;
using VkAnalyzer.Interfaces;
using VkAnalyzer.WebApp.Models;

namespace VkAnalyzer.WebApp.Controllers
{
	/// <inheritdoc />
	/// <summary />
	[Route("api/[controller]")]
	[ApiController]
	public class TrackerController : ControllerBase
	{
		private readonly ITracker _tracker;
		private readonly IUserInfoSource _userSource;
		private readonly IUserInfoRepository _userInfoRepository;
		private readonly IUserRepository _userRepository;

		/// <summary />
		/// <param name="tracker"></param>
		/// <param name="userSource"></param>
		/// <param name="userInfoRepository"></param>
		/// <param name="userRepository"></param>
		public TrackerController(ITracker tracker,
			IUserInfoSource userSource,
			IUserInfoRepository userInfoRepository,
			IUserRepository userRepository)
		{
			_tracker = tracker ?? throw new ArgumentNullException(nameof(tracker));
			_userSource = userSource ?? throw new ArgumentNullException(nameof(userSource));
			_userInfoRepository = userInfoRepository ?? throw new ArgumentNullException(nameof(userInfoRepository));
			_userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
		}

		/// <summary />
		/// <param name="request"></param>
		/// <returns></returns>
		[HttpPost("addUsers")]
		public async Task<BaseResponse> AddUsers([FromBody] AddUsersRequest request)
		{
			var userIds = (await _userRepository.GetUsersAsync()).ToList().Select(u => u.Id);
			var newUserIds = request.Ids
				.Except(request.Ids.Intersect(userIds))
				.Distinct()
				.ToList();

			var newUsers = await _userSource.GetUsersInfo(newUserIds);

			foreach (var user in newUsers)
			{
				await _userRepository.AddUserAsync(new User
				{
					Id = user.Id,
					AddedDateTime = DateTime.Now,
					FirstName = user.FirstName,
					LastName = user.LastName
				});
			}

			_tracker.AddUsers(newUserIds);

			return new BaseSuccessResponse();
		}

		/// <summary />
		/// <returns></returns>
		[HttpGet("usersCount")]
		public async Task<BaseResponse<int>> GetUsersCount()
		{
			return new BaseSuccessResponse<int>
			{
				Data = await _userRepository.GetUsersCountAsync()
			};
		}

		/// <summary />
		/// <returns></returns>
		[HttpGet("users")]
		public async Task<BaseResponse<IEnumerable<UserInfo>>> GetUsers()
		{
			var users = (await _userRepository.GetUsersAsync()).Select(u => new UserInfo
			{
				Id = u.Id,
				FirstName = u.FirstName,
				LastName = u.LastName,
				Photo = u.Photo,
				ScreenName = u.ScreenName
			}).ToList();
			
			return new BaseSuccessResponse<IEnumerable<UserInfo>>
			{
				Data = users
			};
		}

		/// <summary />
		/// <param name="id"></param>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <returns></returns>
		[HttpGet("getdata")]
		public async Task<BaseResponse<UserOnlineData>> GetUserOnlineData(long id, long? from = null, long? to = null)
		{
			var dateFrom = from != null
				? DateTimeOffset.FromUnixTimeMilliseconds(from.Value)
					.DateTime.ToLocalTime()
				: DateTime.Now.AddMonths(-1);

			var dateTo = to != null
				? DateTimeOffset.FromUnixTimeMilliseconds(to.Value)
					.DateTime.ToLocalTime()
				: DateTime.Now;

			var result = await _userInfoRepository.ReadDataAsync(id, dateFrom, dateTo);

			var fixedInfos = result.OnlineInfos.ToList();

			for (var i = 0; i < fixedInfos.Count - 1; i++)
			{
				if (fixedInfos[i].Date < fixedInfos[i + 1].Date) continue;

				fixedInfos.RemoveAt(i);
				i--;
			}

			result.OnlineInfos = fixedInfos;

			return new BaseSuccessResponse<UserOnlineData>
			{
				Data = result
			};
		}
	}
}
