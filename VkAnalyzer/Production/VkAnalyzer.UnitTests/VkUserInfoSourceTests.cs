﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using VkAnalyzer.BE;
using VkAnalyzer.BL;
using VkNet.Abstractions;
using VkNet.Enums.Filters;
using VkNet.Enums.SafetyEnums;
using VkNet.Model.RequestParams;
using VkNet.Utils;
using User = VkNet.Model.User;

namespace VkAnalyzer.UnitTests
{
	[TestFixture]
	public class VkUserInfoSourceTests
	{
		private IVkApi _vkApi;
		private IUsersCategory _usersCategory;
		private VkAnalyzerSettings _vkAnalyzerSettings;

		[SetUp]
		public void Init()
		{
			_vkApi = new Mock<IVkApi>().Object;
			_usersCategory = new Mock<IUsersCategory>().Object;
			_vkAnalyzerSettings = new VkAnalyzerSettings { AppId = "1" };
		}

		[Test]
		[Category("Unit")]
		public async Task GetOnlineInfo_GetCalled()
		{
			// Arrange
			Mock.Get(_vkApi).Setup(x => x.Users).Returns(_usersCategory);
			Mock.Get(_usersCategory)
				.Setup(c => c.GetAsync(It.IsAny<IEnumerable<long>>(),
					It.IsAny<ProfileFields>(),
					It.IsAny<NameCase>(),
					It.IsAny<bool>()))
				.ReturnsAsync(new ReadOnlyCollection<User>(new List<User>()));

			var vk = new VkUserInfoSource(_vkAnalyzerSettings, _vkApi);
			
			// Act
			await vk.GetOnlineInfo(new long[] { 1 });

			// Assert
			Mock.Get(_usersCategory).VerifyAll();
		}

		[Test]
		[Category("Unit")]
		public async Task SearchUsers_GetCalled()
		{
			// Arrange
			Mock.Get(_vkApi).Setup(x => x.Users).Returns(_usersCategory);
			Mock.Get(_usersCategory)
				.Setup(c => c.GetAsync(It.IsAny<IEnumerable<long>>(),
					It.IsAny<ProfileFields>(),
					It.IsAny<NameCase>(),
					It.IsAny<bool>()))
				.ReturnsAsync(new ReadOnlyCollection<User>(new List<User>()));

			var vk = new VkUserInfoSource(_vkAnalyzerSettings, _vkApi);
			
			// Act
			await vk.SearchUsers("id1");

			// Assert
			Mock.Get(_usersCategory).VerifyAll();
		}

		[Test]
		[Category("Unit")]
		public async Task SearchUsers_SearchCalled()
		{
			// Arrange
			Mock.Get(_vkApi).Setup(x => x.Users).Returns(_usersCategory);
			Mock.Get(_usersCategory)
				.Setup(c => c.SearchAsync(It.IsAny<UserSearchParams>()))
				.ReturnsAsync(new VkCollection<User>(0, new List<User>()));

			var vk = new VkUserInfoSource(_vkAnalyzerSettings, _vkApi);
			
			// Act
			await vk.SearchUsers("hello_world");

			// Assert
			Mock.Get(_usersCategory).VerifyAll();
		}
	}
}
