using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using NSwag.AspNetCore;
using VkAnalyzer.BE;
using VkAnalyzer.BE.Mongo;
using VkAnalyzer.BL;
using VkAnalyzer.Interfaces;
using VkAnalyzer.WebApp.Extensions;
using VkAnalyzer.WebApp.Middlewares;
using VkAnalyzer.WebApp.Settings;
using VkAnalyzer.WebApp.Validation;

namespace VkAnalyzer.WebApp
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.Configure<VkAnalyzerSettings>(Configuration.GetSection(nameof(VkAnalyzerSettings)));
			services.Configure<MongoConnectionSettings>(Configuration.GetSection(nameof(MongoConnectionSettings)));
			services.Configure<SqlRepositorySettings>(Configuration.GetSection(nameof(SqlRepositorySettings)));
			services.Configure<FileRepositorySettings>(Configuration.GetSection(nameof(FileRepositorySettings)));
			services.Configure<UserInfoSourceMode>(Configuration.GetSection(nameof(UserInfoSourceMode)));
			services.Configure<RepositoryMode>(Configuration.GetSection(nameof(RepositoryMode)));

			//services.AddLogger();

			services.AddRepository();
			services.AddUserInfoSource();

			services.AddSingleton<ITracker, Tracker>();

			services.AddMvc(options =>
			{
				options.Filters.Add(typeof(ValidatorActionFilter));
			}).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

			// Register the Swagger services
			services.AddSwaggerDocument();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app,
			IHostingEnvironment env,
			ITracker tracker,
			IUserRepository userRepository,
			IApplicationLifetime lifetime,
			Interfaces.ILogger logger)
		{
			lifetime.ApplicationStopping.Register(LogManager.Shutdown);
			Task.Factory.StartNew(async () =>
			{
				var userIds = (await userRepository.GetUsersAsync()).Select(u => u.Id);
				tracker.AddUsers(userIds);
				await tracker.Start();
			});

			if (env.IsDevelopment())
			{
				app.UseCors(builder => builder
					.AllowAnyMethod()
					.AllowAnyHeader()
					.WithOrigins("http://localhost:3000")
					.AllowCredentials()
				);
			}
			else
			{
				//app.UseHsts();
			}

			app.UseForwardedHeaders(new ForwardedHeadersOptions
			{
				ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
			});

			app.UseMiddleware<ExceptionMiddleware>();

			//app.UseHttpsRedirection();

			app.UseDefaultFiles();
			app.UseStaticFiles();

			app.UseSwagger();
#pragma warning disable 618
			app.UseSwaggerUi();
#pragma warning restore 618

			app.UseMvc(routes =>
			{
				routes.MapRoute("api", "api/{controller}/{action}/{id?}");
			});
		}
	}
}
