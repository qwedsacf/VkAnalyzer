import { call, put, takeLatest, fork, take, cancel } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { FIND_USERS, ADD_USER } from './constants';
import {
  findUsersSuccess,
  findUsersError,
  addUserSuccess,
  addUserError,
} from './actions';
import UsersService from '../../services/users';
import { getUsers } from '../HomePage/actions';

export default function* defaultSaga() {
  yield fork(commonRequests);
  yield fork(watchInput);
}

function* commonRequests() {
  yield takeLatest(ADD_USER, addUserFlow);
}

function* watchInput() {
  let task;
  while (true) {
    const { text } = yield take(FIND_USERS);
    if (task) {
      yield cancel(task);
    }
    task = yield fork(findUsersFlow, text);
  }
}

function* findUsersFlow(text) {
  try {
    yield delay(2000);
    const response = yield call(UsersService.findUsers, text);
    if (response.success) {
      yield put(findUsersSuccess(response.data));
    } else {
      yield put(findUsersError(response.errorMessage));
    }
  } catch (error) {
    yield put(findUsersError(error.message));
  }
}

function* addUserFlow({ id }) {
  try {
    const response = yield call(UsersService.addUsers, [id]);
    if (response.success) {
      yield put(addUserSuccess());
      yield put(getUsers());
    } else {
      yield put(addUserError(response.errorMessage));
    }
  } catch (error) {
    yield put(addUserError(error.message));
  }
}
