// склонение имен существительных
export const getNoun = (count, one, two, five) => {
  let number = Math.abs(count);
  number %= 100;
  if (number >= 5 && number <= 20) {
    return five;
  }
  number %= 10;
  if (number === 1) {
    return one;
  }
  if (number >= 2 && number <= 4) {
    return two;
  }
  return five;
};

// спряжение глаголов
export const getVerb = (count, one, many) => {
  let number = Math.abs(count);
  number %= 100;
  if (number >= 5 && number <= 20) {
    return many;
  }
  number %= 10;
  if (number === 1) {
    return one;
  }
  return many;
};
