import React from 'react';
import PropTypes from 'prop-types';
import { Input, Loader } from 'semantic-ui-react';
import Pagination from '../Pagination';
import {
  Row,
  Rows,
  DivWithBottmMargin as DivWithBottomMargin,
} from './Wrappers';

class SelectUsers extends React.PureComponent {
  state = {
    filter: {
      page: 1,
      name: null,
    },
  };

  onFilterChange = changedFields => {
    const { onFilterChange } = this.props;
    const { filter } = this.state;

    const newFilter = Object.assign(filter, changedFields);

    onFilterChange(newFilter);
    this.setState(newFilter);
  };

  onPageChange = (e, { activePage }) =>
    this.onFilterChange({ page: activePage });

  onUserNameChange = (e, { value }) =>
    this.onFilterChange({ page: 1, name: value });

  render() {
    const { users, totalPages, onSelectUser, loading } = this.props;

    const { filter } = this.state;

    const noUsers = !(loading || (users && users.size));

    return (
      <div>
        <DivWithBottomMargin>
          <Input placeholder="Введите имя" onChange={this.onUserNameChange} />
        </DivWithBottomMargin>
        <DivWithBottomMargin>
          <Rows>
            {noUsers ? (
              <div>Пользователей не найдено.</div>
            ) : (
              <>
                <Loader active={loading} />

                {users &&
                  users.map(u => (
                    <Row
                      key={u.get('id')}
                      onClick={() => onSelectUser(u.get('id'))}
                    >
                      {u.get('firstName')} {u.get('lastName')}
                    </Row>
                  ))}
              </>
            )}
          </Rows>
        </DivWithBottomMargin>
        {totalPages > 1 && (
          <DivWithBottomMargin>
            <Pagination
              activePage={filter.page}
              onPageChange={this.onPageChange}
              totalPages={totalPages}
            />
          </DivWithBottomMargin>
        )}
      </div>
    );
  }
}

SelectUsers.propTypes = {
  users: PropTypes.object,
  totalPages: PropTypes.number,
  onFilterChange: PropTypes.func,
  onSelectUser: PropTypes.func,
  loading: PropTypes.bool,
};

export default SelectUsers;
