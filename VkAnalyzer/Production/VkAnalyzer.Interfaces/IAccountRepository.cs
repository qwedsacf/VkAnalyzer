﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace VkAnalyzer.Interfaces
{
	public interface IAccountRepository
	{
		IEnumerable<long> GetTrackedUsers(Guid id);
		Task<IEnumerable<long>> GetTrackedUsersAsync(Guid id);

		void AddTrackedUsers(Guid id, IEnumerable<long> users);
		Task AddTrackedUsersAsync(Guid id, IEnumerable<long> users);
	}
}
