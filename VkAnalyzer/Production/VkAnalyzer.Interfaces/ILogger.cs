﻿using System;
using System.Threading.Tasks;

namespace VkAnalyzer.Interfaces
{
	public interface ILogger
	{
		void Log(Exception exception, string additionalInfo = null);
		Task LogAsync(Exception exception, string additionalInfo = null);
	}
}
