using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using VkAnalyzer.BL;
using VkAnalyzer.Interfaces;

namespace VkAnalyzer.UnitTests
{
	[TestFixture]
	public class TrackerTests
	{
		private IUserInfoSource _userInfoSource;
		private IUserInfoRepository _userInfoRepository;
		private ILogger _logger;

		[SetUp]
		public void Init()
		{
			_userInfoSource = new Mock<IUserInfoSource>(MockBehavior.Strict).Object;
			_userInfoRepository = new Mock<IUserInfoRepository>(MockBehavior.Strict).Object;
			_logger = new Mock<ILogger>(MockBehavior.Strict).Object;
		}

		[Test]
		[Category("Unit")]
		public void AddUsers_UserAdded()
		{
			// Arrange
			var tracker = new Tracker(_userInfoSource, _userInfoRepository, _logger);

			// Act
			tracker.AddUsers(new long[] { 1 });
			var actual = tracker.GetUsers().ToList();
			var expected = new List<long> { 1 };

			// Assert
			TestHelpers.CompareCollections(expected, actual);
		}

		[Test]
		[Category("Unit")]
		public void AddUsers_UsersNotDuplicated()
		{
			// Arrange
			var tracker = new Tracker(_userInfoSource, _userInfoRepository, _logger);

			// Act
			tracker.AddUsers(new long[] { 1, 2 });
			tracker.AddUsers(new long[] { 2, 3 });

			var actual = tracker.GetUsers().ToList();
			var expected = new List<long> { 1, 2, 3 };

			// Assert
			TestHelpers.CompareCollections(expected, actual);
		}

		[Test]
		[Category("Unit")]
		public void RemoveUser_UserRemoved()
		{
			// Arrange
			var tracker = new Tracker(_userInfoSource, _userInfoRepository, _logger);

			// Act
			tracker.AddUsers(new long[] { 1, 2 });
			tracker.RemoveUser(2);

			var actual = tracker.GetUsers().ToList();
			var expected = new List<long> { 1 };

			// Assert
			TestHelpers.CompareCollections(expected, actual);
		}

		[Test]
		[Category("Unit")]
		public void RemoveUser_RemoveNotExistedUser()
		{
			// Arrange
			var tracker = new Tracker(_userInfoSource, _userInfoRepository, _logger);

			// Act
			tracker.AddUsers(new long[] { 1, 2 });
			tracker.RemoveUser(3);

			var actual = tracker.GetUsers().ToList();
			var expected = new List<long> { 1, 2 };

			// Assert
			TestHelpers.CompareCollections(expected, actual);
		}
	}
}