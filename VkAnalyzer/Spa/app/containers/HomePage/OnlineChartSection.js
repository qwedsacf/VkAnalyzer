import React from 'react';
import PropTypes from 'prop-types';
import { Button, Loader } from 'semantic-ui-react';
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';

import OnlineChart from '../../components/OnlineChart';
import 'react-dates/lib/css/_datepicker.css';
import {
  ChoosePeriodWrapper,
  ChartWrapper,
  DateRangePickerWrapper,
} from './Wrappers';

class OnlineChartSection extends React.PureComponent {
  state = {
    startDate: null,
    endDate: null,
    focusedInput: null,
  };

  render() {
    const { onlineData, getData } = this.props;
    const { startDate, endDate, focusedInput } = this.state;

    return (
      <div style={{ position: 'relative' }}>
        <Loader active={onlineData.loading} />
        {onlineData.data && (
          <div>
            <div>График онлайна:</div>
            <ChartWrapper>
              {!!onlineData.data.onlineInfos && (
                <OnlineChart data={onlineData.data.onlineInfos} />
              )}
            </ChartWrapper>
            <div style={{ marginBottom: '10px' }}>Временной интервал:</div>
            <ChoosePeriodWrapper>
              <DateRangePickerWrapper>
                <DateRangePicker
                  startDate={startDate}
                  startDateId="your_unique_start_date_id"
                  endDate={endDate}
                  endDateId="your_unique_end_date_id"
                  onDatesChange={({ startDate, endDate }) =>
                    this.setState({ startDate, endDate })
                  }
                  focusedInput={focusedInput}
                  onFocusChange={focusedInput =>
                    this.setState({ focusedInput })
                  }
                  startDatePlaceholderText="От"
                  endDatePlaceholderText="До"
                  noBorder
                  showClearDates
                  small
                  isOutsideRange={() => false}
                />
              </DateRangePickerWrapper>
              <Button
                onClick={() =>
                  getData(
                    startDate && Math.round(startDate),
                    endDate && Math.round(endDate),
                  )
                }
                size="medium"
              >
                Обновить
              </Button>
            </ChoosePeriodWrapper>
          </div>
        )}
      </div>
    );
  }
}

OnlineChartSection.propTypes = {
  onlineData: PropTypes.object,
  getData: PropTypes.func,
};

export default OnlineChartSection;
