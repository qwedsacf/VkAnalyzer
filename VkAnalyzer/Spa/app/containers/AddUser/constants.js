export const FIND_USERS = 'app/AddUser/FIND_USERS';
export const FIND_USERS_SUCCESS = 'app/AddUser/FIND_USERS_SUCCESS';
export const FIND_USERS_ERROR = 'app/AddUser/FIND_USERS_ERROR';

export const ADD_USER = 'app/AddUser/ADD_USER';
export const ADD_USER_SUCCESS = 'app/AddUser/ADD_USER_SUCCESS';
export const ADD_USER_ERROR = 'app/AddUser/ADD_USER_ERROR';
