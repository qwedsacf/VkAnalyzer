import styled from 'styled-components';

export const Rows = styled.div`
  height: 300px;
  overflow-y: auto;
  position: relative;
`;

export const Row = styled.div`
  padding: 1em;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
  cursor: pointer;

  :hover {
    background: #fff !important;
    box-shadow: 0 3px 10px 0 rgba(75, 129, 185, 0.15);
  }
`;

export const DivWithBottmMargin = styled.div`
  margin-bottom: 1em;
`;
