import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectAddUserDomain = state => state.get('addUser', initialState);

const makeSelectAddUser = () =>
  createSelector(selectAddUserDomain, substate => substate.toJS());

const makeSelectFoundedUsers = () =>
  createSelector(selectAddUserDomain, substate => substate.get('foundedUsers'));

export { makeSelectAddUser, makeSelectFoundedUsers };
