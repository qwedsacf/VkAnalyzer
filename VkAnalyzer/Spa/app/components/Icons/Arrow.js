import React from 'react';
import PropTypes from 'prop-types';

const Arrow = ({
  width = 24,
  height = 24,
  onClick,
  direction = 'left',
  className,
}) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    onClick={onClick}
    className={className}
    viewBox="0 0 24 24"
  >
    {direction === 'left' && (
      <path d="M15.41 16.59L10.83 12l4.58-4.59L14 6l-6 6 6 6 1.41-1.41z" />
    )}
    {direction === 'right' && (
      <path d="M8.59 16.59L13.17 12 8.59 7.41 10 6l6 6-6 6-1.41-1.41z" />
    )}
    <path fill="none" d="M0 0h24v24H0V0z" />
  </svg>
);

Arrow.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  onClick: PropTypes.func,
  direction: PropTypes.string,
  className: PropTypes.string,
};
export default Arrow;
