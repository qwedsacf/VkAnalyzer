﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VkAnalyzer.BE;

namespace VkAnalyzer.Interfaces
{
    public interface IUserRepository
    {
	    int GetUsersCount();
	    Task<int> GetUsersCountAsync();

		IEnumerable<User> GetUsers(IEnumerable<long> ids = null);
        Task<IEnumerable<User>> GetUsersAsync(IEnumerable<long> ids = null);

        void AddUser(User user);
        Task AddUserAsync(User id);
		
		bool IsUserTracked(long id);
		Task<bool> IsUserTrackedAsync(long id);
	}
}
