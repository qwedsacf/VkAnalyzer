﻿namespace VkAnalyzer.BE
{
	public class TrackerSettings
	{
		public ulong AppId { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
	}
}
