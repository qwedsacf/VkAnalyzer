import styled, { css } from 'styled-components';
import { Container, Header, Divider, Grid } from 'semantic-ui-react';
import Block from '../../components/Block';

export const MainContainer = styled(Container)`
  padding: 3rem;
`;

export const MainTitle = styled(Header)`
  text-align: center;
  margin-bottom: 2em;
`;

export const SectionsDivider = styled(Divider)`
  &&& {
    font-size: 18px;
    margin: 3em 0 2em;
  }
`;

export const OverflowedBlock = styled(Block)`
  overflow: auto;
`;

export const ChooseDateWrapper = styled(Block)`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const ChoosePeriodWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const ChartWrapper = styled.div`
  margin: 2em 0;
`;

export const DateRangePickerWrapper = styled.div`
  display: inline-block;
  button.DayPickerKeyboardShortcuts_buttonReset {
    display: none;
  }
  input.DateInput_input {
    text-align: center;
  }
  button.DateRangePickerInput_clearDates {
    padding-top: 4px;
    margin-right: 6px;
  }
  padding: 0.1em;
  border: 1px solid rgba(34, 36, 38, 0.15);
  border-radius: 0.28571429rem;
  margin-right: 1em;
`;

export const Border = css`
  border: 1px solid #eee;
  border-radius: 5px;
`;

export const BorderedGridColumn = styled(Grid.Column)`
  &&& {
    ${Border};
  }
`;
