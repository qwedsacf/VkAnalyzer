﻿using System;
using MongoDB.Bson;

namespace VkAnalyzer.BE.Mongo
{
	public class MongoLog
	{
		public ObjectId Id { get; set; }
		public DateTime DateTime { get; set; }
		public string Message { get; set; }
		public string StackTrace { get; set; }
		public string AdditionalInfo { get; set; }
	}
}
