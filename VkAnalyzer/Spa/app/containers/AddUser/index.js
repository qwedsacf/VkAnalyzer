import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, bindActionCreators } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import { Button, Grid, Input } from 'semantic-ui-react';
import { findUsers, addUser } from './actions';
import { makeSelectAddUser, makeSelectFoundedUsers } from './selectors';
import reducer from './reducer';
import saga from './saga';
import { makeSelectAllUsers } from '../HomePage/selectors';

class AddUsers extends React.Component {
  isUserTracked = id => {
    const { allUsers } = this.props;
    const founded = allUsers.find(u => u.get('id') === id);
    return founded;
  };

  findUsers = (e, data) => {
    const { findUsers } = this.props;
    findUsers(data.value);
  };

  render() {
    const { foundedUsers, addUser } = this.props;

    return (
      <Grid>
        <Grid.Column computer={8} tablet={16}>
          <div>Вставьте ссылку или введите имя</div>
          <div style={{ display: 'flex' }}>
            <Input fluid onChange={this.findUsers} style={{ flexGrow: '1' }} />
            <Button style={{ marginLeft: '20px' }}>Поиск</Button>
          </div>
          <Grid style={{ marginTop: '20px' }}>
            {foundedUsers &&
              foundedUsers.map(user => (
                <Grid.Column computer={4} mobile={6} key={user.get('id')}>
                  <div style={{ minHeight: '38px' }}>
                    <a
                      href={`https://vk.com/id${user.get('id')}`}
                      target="_blank"
                    >
                      {user.get('firstName')} {user.get('lastName')}
                    </a>
                  </div>
                  <img src={user.get('photo')} alt={user.get('firstName')} />
                  {!this.isUserTracked(user.get('id')) && (
                    <div style={{ marginTop: '10px' }}>
                      <Button onClick={() => addUser(user.get('id'))}>
                        Добавить
                      </Button>
                    </div>
                  )}
                </Grid.Column>
              ))}
          </Grid>
        </Grid.Column>
        <Grid.Column computer={8} tablet={16} />
      </Grid>
    );
  }
}

AddUsers.propTypes = {
  // selectors
  foundedUsers: PropTypes.object,
  allUsers: PropTypes.object,

  // actions
  findUsers: PropTypes.func,
  addUser: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  addUser: makeSelectAddUser(),
  foundedUsers: makeSelectFoundedUsers(),
  allUsers: makeSelectAllUsers(),
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      findUsers,
      addUser,
    },
    dispatch,
  );
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'addUser', reducer });
const withSaga = injectSaga({ key: 'addUser', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AddUsers);
