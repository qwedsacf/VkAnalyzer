﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace VkAnalyzer.BE
{
	public class UserOnlineInfo
	{
		[BsonRepresentation(BsonType.ObjectId)]
		public long Id { get; set; }

		[BsonRepresentation(BsonType.DateTime)]
		public DateTime DateTime { get; set; }

		[BsonRepresentation(BsonType.Binary)]
		public OnlineInfo OnlineInfo { get; set; }
	}
}
