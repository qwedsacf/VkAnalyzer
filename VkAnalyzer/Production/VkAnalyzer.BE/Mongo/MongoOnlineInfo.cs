﻿using System;

namespace VkAnalyzer.BE.Mongo
{
	public class MongoOnlineInfo
	{
		public DateTime DateTime { get; set; }
		public OnlineInfo OnlineInfo { get; set; }
	}
}