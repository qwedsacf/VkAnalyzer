﻿using System.Collections.Generic;

namespace VkAnalyzer.Interfaces
{
	public interface ITrackerManager
	{
		void AddUsers(IEnumerable<long> ids);
		void AddTrackers(IEnumerable<ITracker> tracker);
	}
}
