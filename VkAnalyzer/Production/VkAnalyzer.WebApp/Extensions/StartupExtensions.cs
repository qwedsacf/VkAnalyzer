﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization.Conventions;
using NLog;
using VkAnalyzer.BE;
using VkAnalyzer.BE.Mongo;
using VkAnalyzer.BL;
using VkAnalyzer.BL.File;
using VkAnalyzer.BL.Sql;
using VkAnalyzer.Interfaces;
using VkAnalyzer.WebApp.Settings;
using VkNet.NLog.Extensions.Logging;
using VkNet.NLog.Extensions.Logging.Extensions;
using ILogger = VkAnalyzer.Interfaces.ILogger;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace VkAnalyzer.WebApp.Extensions
{
	public static class StartupExtensions
	{
		public static void AddVkUserInfoSource(this IServiceCollection collection)
		{
			var serviceProvider = collection.BuildServiceProvider();
			var vkAnalyzerSettings = serviceProvider.GetService<IOptions<VkAnalyzerSettings>>().Value;
			var trackerInfo = serviceProvider.GetService<ITrackerRepository>().GetTrackers().FirstOrDefault();

			if (!ulong.TryParse(vkAnalyzerSettings.AppId, out _))
			{
				if (trackerInfo == null)
				{
					collection.AddDummyUserInfoSource();
					return;
				}

				vkAnalyzerSettings = new VkAnalyzerSettings
				{
					AppId = trackerInfo.AppId.ToString(),
					VkUserLogin = trackerInfo.Login,
					VkUserPassword = trackerInfo.Password
				};
			}
			
			collection.AddSingleton<IUserInfoSource, VkUserInfoSource>(x => new VkUserInfoSource(vkAnalyzerSettings));
		}

		public static void AddDummyUserInfoSource(this IServiceCollection collection)
		{
			collection.AddSingleton<IUserInfoSource, DummyUsersInfoSource>();
		}

		public static void AddUserInfoSource(this IServiceCollection collection)
		{
			var mode = collection.BuildServiceProvider().GetService<IOptions<UserInfoSourceMode>>().Value;

			switch (mode.Mode)
			{
				case "Vk":
					collection.AddVkUserInfoSource();
					break;
				case "Dummy":
					collection.AddDummyUserInfoSource();
					break;
				default:
					throw new ArgumentException(nameof(UserInfoSourceMode));
			}
		}


		public static void AddFileRepository(this IServiceCollection collection)
		{
			var serviceProvider = collection.BuildServiceProvider();
			var fileRepositorySettings = serviceProvider.GetService<IOptions<FileRepositorySettings>>().Value;
			var repository  = new FileUserRepository(fileRepositorySettings);

			collection.AddSingleton<IUserRepository, FileUserRepository>(x => repository);
		}

		public static void AddSqlRepository(this IServiceCollection collection)
		{
			var serviceProvider = collection.BuildServiceProvider();

			var repositorySettings = serviceProvider.GetService<IOptions<SqlRepositorySettings>>().Value;

			collection.AddDbContext<UsersDbContext>(o => o.UseSqlServer(repositorySettings.Connection));

			var options = serviceProvider.GetRequiredService<DbContextOptions<UsersDbContext>>();

			var repository = new SqlRepository(options);

			collection.AddSingleton<IUserInfoRepository, SqlRepository>(x => repository);
			collection.AddSingleton<IUserRepository, SqlRepository>(x => repository);
		}

		public static void AddMongoRepository(this IServiceCollection collection)
		{
			// register convention
			var conventionPack = new ConventionPack { new CamelCaseElementNameConvention() };
			ConventionRegistry.Register("camelCase", conventionPack, t => true);

			// get settings
			var serviceProvider = collection.BuildServiceProvider();
			var repositorySettings = serviceProvider.GetService<IOptions<MongoConnectionSettings>>().Value;

			var repository = new MongoRepository(repositorySettings);
			collection.AddRepositories(repository);
		}

		public static void AddRepositories<T>(this IServiceCollection collection, T repository)
			where T : class, IUserRepository, IUserInfoRepository, ITrackerRepository, IAccountRepository, ILogger
		{
			collection.AddSingleton<IUserRepository, T>(x => repository);
			collection.AddSingleton<IUserInfoRepository, T>(x => repository);
			collection.AddSingleton<ITrackerRepository, T>(x => repository);
			collection.AddSingleton<IAccountRepository, T>(x => repository);
			collection.AddSingleton<ILogger, T>(x => repository);
		}

		public static void AddRepository(this IServiceCollection collection)
		{
			var mode = collection.BuildServiceProvider().GetService<IOptions<RepositoryMode>>().Value;

			switch (mode.Mode)
			{
				case "Sql":
					collection.AddSqlRepository();
					break;
				case "File":
					collection.AddFileRepository();
					break;
				case "Mongo":
					collection.AddMongoRepository();
					break;
				default:
					throw new ArgumentException(nameof(RepositoryMode));
			}
		}

		public static void AddLogger(this IServiceCollection services)
		{
			services.AddSingleton<ILoggerFactory, LoggerFactory>();
			services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));
			services.AddLogging(builder =>
			{
				builder.ClearProviders();
				builder.SetMinimumLevel(LogLevel.Trace);
				builder.AddNLog(new NLogProviderOptions
				{
					CaptureMessageProperties = true,
					CaptureMessageTemplates = true
				});
			});
			LogManager.LoadConfiguration("nlog.config");
		}
	}
}
