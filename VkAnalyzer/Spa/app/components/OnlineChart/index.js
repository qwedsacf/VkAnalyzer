import React from 'react';
import PropTypes from 'prop-types';

// Pond
import { TimeSeries, TimeRangeEvent, TimeRange } from 'pondjs';

// Imports from the charts library
import {
  ChartContainer,
  ChartRow,
  Charts,
  EventChart,
  Resizable,
} from 'react-timeseries-charts';

function outageEventStyleFunc(event, state) {
  const status = event._d.getIn(['data', 'type']);
  let color;
  switch (status) {
    case 0:
      color = 'red';
      break;
    case 2:
      color = 'blue';
      break;
    case 3:
      color = 'green';
      break;
    case 4:
      color = 'yellow';
      break;
    default:
      color = 'black';
      break;
  }

  switch (state) {
    case 'normal':
      return {
        fill: color,
      };
    case 'hover':
      return {
        fill: color,
        opacity: 0.4,
      };
    case 'selected':
      return {
        fill: color,
      };
    default:
      return {};
  }
}

class OnlineChart extends React.PureComponent {
  state = {
    timerange: null,
    series: null,
  };

  componentDidMount() {
    const { data } = this.props;
    this.setSeries(data);
  }

  componentDidUpdate(prevProps) {
    const { data } = this.props;
    if (prevProps.data === data) return;

    this.setSeries(data);
  }

  setSeries = data => {
    const series = this.mapOnlineInfosToSeries(data);
    this.setState({ series, timerange: series.timerange() });
  };

  handleTimeRangeChange = timerange => {
    this.setState({ timerange });
  };

  mapOnlineInfosToSeries = ranges => {
    const timeRanges = ranges.map(
      ({ startTime, endTime, ...data }) =>
        new TimeRangeEvent(
          new TimeRange(new Date(startTime), new Date(endTime)),
          data,
        ),
    );

    const series = new TimeSeries({ name: 'online', events: timeRanges });
    return series;
  };

  render() {
    const { timerange, series } = this.state;
    const { data } = this.props;

    if (!data) return null;

    if (data.length === 0)
      return (
        <div>В указанный промежуток времени пользователь не был в сети.</div>
      );

    return (
      <div>
        <Resizable>
          <ChartContainer
            timeRange={timerange}
            enablePanZoom
            onTimeRangeChanged={this.handleTimeRangeChange}
          >
            <ChartRow height="60">
              <Charts>
                <EventChart
                  series={series}
                  size={45}
                  style={outageEventStyleFunc}
                  // label={e => e.get('value')}
                />
              </Charts>
            </ChartRow>
          </ChartContainer>
        </Resizable>
      </div>
    );
  }
}

OnlineChart.propTypes = {
  data: PropTypes.array,
};

export default OnlineChart;
