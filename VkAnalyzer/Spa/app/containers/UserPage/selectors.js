import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectUserPageDomain = state => state.get('userPage', initialState);
const makeSelectUserPage = () =>
  createSelector(selectUserPageDomain, substate => substate.toJS());

export default makeSelectUserPage;
export { selectUserPageDomain };
