﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VkAnalyzer.BE;
using VkAnalyzer.Interfaces;

namespace VkAnalyzer.BL.File
{
	public class FileUserRepository : IUserRepository
	{
		private readonly FileRepositorySettings _fileRepositorySettings;
		public FileUserRepository(FileRepositorySettings fileRepositorySettings)
		{
			_fileRepositorySettings = fileRepositorySettings ?? throw new ArgumentNullException(nameof(fileRepositorySettings));
		}

		public int GetUsersCount()
		{
			return System.IO.File.ReadAllLines(_fileRepositorySettings.FileUsersRepositoryPath).Length;
		}

		public Task<int> GetUsersCountAsync()
		{
			return Task.FromResult(GetUsersCount());
		}

		public IEnumerable<User> GetUsers(IEnumerable<long> ids = null)
		{
			return System.IO.File.ReadAllLines(_fileRepositorySettings.FileUsersRepositoryPath).Select(User.Parse);
		}

		public async Task<IEnumerable<User>> GetUsersAsync(IEnumerable<long> ids = null)
		{
			var lines = await System.IO.File.ReadAllLinesAsync(_fileRepositorySettings.FileUsersRepositoryPath);
			return lines.AsParallel().Select(User.Parse);
		}

		public void AddUser(User user)
		{
			FileWriter.WriteInFile(_fileRepositorySettings.FileUsersRepositoryPath, user + Environment.NewLine);
		}

		public async Task AddUserAsync(User user)
		{
			await FileWriter.WriteInFileAsync(_fileRepositorySettings.FileUsersRepositoryPath, user + Environment.NewLine);
		}

		public bool IsUserTracked(long id)
		{
			throw new NotImplementedException();
		}

		public Task<bool> IsUserTrackedAsync(long id)
		{
			throw new NotImplementedException();
		}
	}
}
