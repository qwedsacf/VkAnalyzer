import styled, { css } from 'styled-components';
import { Input } from 'semantic-ui-react';
import Arrow from '../Icons/Arrow';

export const PaginationInput = styled(Input)`
  &&& input {
    border: none;
    padding: 0 0 3px;
    border-bottom: 1px solid black;
    border-radius: 0;
    text-align: center;
    width: 50px;
    margin: 0;
  }
`;

export const PaginationArrow = styled(Arrow)`
  cursor: pointer;
  margin: 0 5px;
`;

export const NoSelect = css`
  -webkit-touch-callout: none; /* iOS Safari */
  -webkit-user-select: none; /* Safari */
  -khtml-user-select: none; /* Konqueror HTML */
  -moz-user-select: none; /* Firefox */
  -ms-user-select: none; /* Internet Explorer/Edge */
  user-select: none; /* Non-prefixed version, currently supported by Chrome and Opera */
`;

export const PaginationWrapper = styled.div`
  ${NoSelect};
`;
