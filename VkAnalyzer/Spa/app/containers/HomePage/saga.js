import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_USERS, GET_DATA, ADD_FRIENDS } from './constants';
import {
  setUsers,
  getUsersError,
  setData,
  getDataError,
  setFriends,
  getFriendsError,
  setUsersCount,
  setUserInfo,
  getUserInfoError,
} from './actions';
import UsersService from '../../services/users';
import { setError } from '../App/actions';

export default function* defaultSaga() {
  yield takeLatest(GET_USERS, getUsersFlow);
  yield takeLatest(GET_DATA, getOnlineDataFlow);
  yield takeLatest(ADD_FRIENDS, addFriendsFlow);
}

function* getUsersFlow() {
  try {
    const { success, data } = yield call(UsersService.getUsersCount);
    if (success) yield put(setUsersCount(data));

    const response = yield call(UsersService.getUsers);
    if (response.success) {
      yield put(setUsers(response.data));
    } else {
      yield put(getUsersError(response.errorMessage));
    }
  } catch (error) {
    yield put(getUsersError(error.message));
  }
}

function* getOnlineDataFlow({ id, from, to }) {
  try {
    const userResponse = yield call(UsersService.getUserInfo, id);
    if (userResponse.success) {
      yield put(setUserInfo(userResponse.data));
    } else {
      yield put(getUserInfoError(userResponse.errorMessage));
    }

    const response = yield call(UsersService.getData, id, from, to);
    if (response.success) {
      yield put(setData(response.data));
    } else {
      yield put(getDataError(response.errorMessage));
    }
  } catch (error) {
    yield put(getDataError(error.message));
  }
}

function* addFriendsFlow({ userId }) {
  try {
    const response = yield call(UsersService.getFriends, userId);
    const { success, data } = response;
    if (success) {
      yield put(setError(`Друзей: ${data.length}`));
      yield put(setFriends(data));
      if (data.length === 0) yield put(setError('Друзей не найдено'));
      yield call(UsersService.addUsers, data.map(i => i.id));
      yield call(getUsersFlow);
    } else {
      yield put(getFriendsError(response.errorMessage));
    }
  } catch (error) {
    yield put(setError(error.message));
  }
}
