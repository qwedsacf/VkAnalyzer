import {
  GET_USERS,
  SET_USERS,
  GET_USERS_ERROR,
  GET_DATA,
  GET_DATA_SUCCESS,
  GET_DATA_ERROR,
  ADD_FRIENDS,
  SET_FRIENDS,
  GET_FRIENDS_ERROR,
  SET_USERS_COUNT,
  USERNAME_CHANGED,
  GET_USER_INFO_ERROR,
  SET_USER_INFO,
  GET_USER_INFO,
  USERS_FILTER_CHANGED,
} from './constants';

export const getUsers = () => ({ type: GET_USERS });
export const setUsers = users => ({ type: SET_USERS, users });
export const getUsersError = error => ({ type: GET_USERS_ERROR, error });

export const getData = (id, from, to) => ({ type: GET_DATA, id, from, to });
export const setData = data => ({ type: GET_DATA_SUCCESS, data });
export const getDataError = error => ({ type: GET_DATA_ERROR, error });

export const addFriends = userId => ({ type: ADD_FRIENDS, userId });
export const setFriends = data => ({ type: SET_FRIENDS, data });
export const getFriendsError = error => ({ type: GET_FRIENDS_ERROR, error });

export const setUsersCount = count => ({ type: SET_USERS_COUNT, count });

export const usernameChanged = name => ({ type: USERNAME_CHANGED, name });

export const getUserInfo = id => ({ type: GET_USER_INFO, id });
export const setUserInfo = info => ({ type: SET_USER_INFO, info });
export const getUserInfoError = error => ({ type: GET_USER_INFO_ERROR, error });

export const usersFilterChanged = filter => ({
  type: USERS_FILTER_CHANGED,
  filter,
});
