import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  PaginationInput,
  PaginationArrow,
  PaginationWrapper,
} from './Wrappers';

function Pagination(props) {
  const { activePage, totalPages, onPageChange } = props;
  const [page, setPage] = useState(1);

  useEffect(
    () => {
      setPage(activePage);
    },
    [activePage],
  );

  const onPageChangeHandler = (e, data) => {
    setPage(data.value);

    let newPage = +data.value;
    if (!newPage || newPage < 1) newPage = 1;
    if (newPage > totalPages) newPage = totalPages;
    if (activePage !== newPage) onPageChange(null, { activePage: newPage });
  };

  return (
    <PaginationWrapper>
      <span>Страница </span>
      <span>
        {activePage > 1 && (
          <PaginationArrow
            direction="left"
            onClick={() => onPageChangeHandler(null, { value: activePage - 1 })}
          />
        )}
        <PaginationInput value={page} onChange={onPageChangeHandler} />
        {activePage < totalPages && (
          <PaginationArrow
            direction="right"
            onClick={() => onPageChangeHandler(null, { value: activePage + 1 })}
          />
        )}
      </span>
      <span> из {totalPages}</span>
    </PaginationWrapper>
  );
}

Pagination.propTypes = {
  activePage: PropTypes.number,
  totalPages: PropTypes.number,
  onPageChange: PropTypes.func,
};

export default Pagination;
